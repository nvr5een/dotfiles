# ~/.tmux.conf

unbind C-b
set -g prefix C-Space

# Change the default $TERM to screen-256color
set -g default-terminal "screen-256color"

# Window and pane numbers start at 1 instead of 0
set -g base-index 1
set -g pane-base-index 1

# Use vi key bindings in copy and choice modes
set -g mode-keys vi

# Use emacs key bindings at command prompt
set -g status-keys emacs

# No delay when <Esc> is pressed
set -s escape-time 1

# Display messages until any key is pressed
set -g display-time 0

# Enable scrolling with mouse
set -g terminal-overrides "xterm*:smcup@:rmcup@"
set -g mouse on

# Reload tmux config file
bind R source-file ~/.tmux.conf \; display-message "Config file reloaded!"

# Split panes with \ and -
bind \ split-window -h
bind - split-window -v

# Split pane in current working directory
bind | split-window -h -c "#{pane_current_path}"
bind _ split-window -v -c "#{pane_current_path}"

# Resize panes with H, J, K, L
bind -r H resize-pane -L 5
bind -r J resize-pane -D 5
bind -r K resize-pane -U 5
bind -r L resize-pane -R 5

# Cycle through windows
bind -r C-h select-window -t :-
bind -r C-l select-window -t :+

# OS specific settings
if-shell "uname | grep -q Darwin" "source-file ~/.tmux/tmux.macos.conf"
if-shell "uname | grep -q Linux" "source-file ~/.tmux/tmux.linux.conf"

# Statusline
set -g status "on"
set -g status-justify "left"
set -g status-left-length "100"
set -g status-right-length "100"
set -g status-bg "colour236"
setw -g window-status-separator ""
setw -g window-status-last-style underscore
set -g status-left "#[fg=colour250,bg=colour236][#S]"
setw -g window-status-format "#[fg=colour243,bg=colour236] #I:#W "
setw -g window-status-current-format "#[fg=colour250,bg=colour236][#I:#W]"
set -g status-right "#[fg=colour250,bg=colour236]#(uptime | rev | cut -d":" -f1 | rev | sed s/,//g ) | %H:%M [#h]"
