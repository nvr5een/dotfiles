# ~/.zsh/aliases.zsh

alias la='ls -A'
alias ll='ls -lA'
alias lr='ls -R' # recursive ls
alias lx='ll -BX' # sort by extension
alias lz='ll -rS' # sort by size
alias lt='ll -rt' # sort by date

alias chgrp='chgrp --preserve-root'
alias chmod='chmod --preserve-root'
alias chown='chown --preserve-root'
alias cp='cp -iv'
alias ln='ln -iv'
alias mv='nocorrect mv -iv'
alias rm='rm -Iv'

alias d='dirs -v'
alias df='df -h'
alias du='du -ch'
alias e='$EDITOR'
alias se='sudoedit'
alias hi='fc -RI' # manually import command history
alias history-stat="history 0 | awk '{print \$2}' | sort | uniq -c | sort -n -r | head"
alias j='jobs -l'
alias mkdir='nocorrect mkdir -pv'
if type setxkbmap &>/dev/null; then
  alias nocaps=' setxkbmap -option ctrl:nocaps'
fi
alias reload='. ~/.zshrc; echo "ZSH config reloaded!"'
alias ta='tmux attach -t'
alias tk='tmux kill-session -t'
alias tn='tmux new -s'
alias unzip-all='unzip \*.zip'
alias weather='curl wttr.in'
if type youtube-dl &>/dev/null; then
  alias ydm='noglob youtube-dl -o ~/Dropbox/Music/YouTube/%(title)s.%(ext)s -x --audio-format "mp3" --audio-quality "0"'
  alias ydv='noglob youtube-dl'
fi

if [[ -e ~/Dropbox ]]; then
  alias db='cd ~/Dropbox; la'
fi
alias dl='cd ~/Downloads; la'
alias pj='cd ~/projects; la'

alias g='git'
alias ga='git add'
alias gb='git branch'
alias gcm='git commit -v -m'
alias gco='git checkout'
alias gcl='git clone'
alias gd='git diff'
alias gl='git pull'
alias glp='git log --pretty=format:"%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset" --abbrev-commit'
alias gmv='git mv -v'
alias gp='git push'
alias grm='git rm'
alias grs='git reset --soft HEAD~1'
alias gs='git status'

if type apt &>/dev/null; then
  alias apti='sudo apt install --no-install-recommends'
  alias aptu='sudo apt update'
  alias aptug='sudo apt upgrade'
  alias aptf='sudo apt full-upgrade'
  alias aptr='sudo apt remove'
  alias aptp='sudo apt purge'
  alias apta='sudo apt autoremove'
  alias apts='apt search'
  alias aptq='apt show'
  alias aptli='apt list --installed | less'
  alias aptlu='apt list --upgradeable'
fi

if type pacman &>/dev/null; then
  if type yay &>/dev/null; then
    alias paci='yay -S' # install package(s) (incl AUR)
    alias pacu='yay -Syu' # sync, then upgrade all packages (incl AUR)
    alias pacs='yay -Ss' # search for package(s) (incl AUR)
    alias pacq='yay -Si' # show package info (incl AUR)
  else
    alias paci='sudo pacman -S' # install package(s)
    alias pacu='sudo pacman -Syu' # sync, then upgrade all packages
    alias pacs='pacman -Ss' # search for package(s)
    alias pacq='pacman -Si' # show package info
  fi
  alias pacr='sudo pacman -R' # remove package(s), retaining config(s) and req depends
  alias pacrr='sudo pacman -Rns' # remove package(s), config(s) and unneeded depends
  alias pacli='pacman -Q | less' # list all packages currently installed
  alias pacll='pacman -Qqm' # list all packages locally installed (AUR)
  alias paco='pacman -Qo' # determine which package owns a given file
  alias pacf='pacman -Ql' # list all files installed by a given package
  alias pacc='sudo pacman -Sc' # delete all not currently installed package files
  alias pacm='makepkg -fsic' # make package from PKGBUILD file in current directory
  alias pacstat='pacman -Q | wc -l' # print number of packages installed
fi
