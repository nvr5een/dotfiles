# ~/.zsh/fzf.zsh

fzf_dir=~/.fzf

if [[ ! -d "$fzf_dir" ]]; then
  git clone --depth 1 https://github.com/junegunn/fzf.git "$fzf_dir"
  "$fzf_dir"/install --bin
fi

if [[ -x "$fzf_dir/bin/fzf" ]]; then
  export PATH="$fzf_dir/bin:$PATH"
  . "$fzf_dir"/shell/completion.zsh
  . "$fzf_dir"/shell/key-bindings.zsh
fi

if type rg &>/dev/null; then
  export FZF_DEFAULT_COMMAND='rg --follow --hidden --files 2> /dev/null'
elif type ag &>/dev/null; then
  export FZF_DEFAULT_COMMAND='ag --follow --hidden -g "" 2> /dev/null'
fi

fm() {
  local file
  file=$(find -L ~/Downloads ~/Music -iname "*.mp3" -o -iname "*.flac" \
         -o -iname "*.wav" 2> /dev/null | fzf) &&
  mpv "$file" && fm
}

fe() {
  local out file key
  IFS=$'\n' out=($(fzf --query="$1" --exit-0 --expect=ctrl-g,ctrl-e))
  key=$(head -1 <<< "$out")
  file=$(head -2 <<< "$out" | tail -1)
  if [ -n "$file" ]; then
    [ "$key" = ctrl-g ] && gvim --remote-silent "$file" || ${EDITOR:-vim} "$file"
  fi
}

fdir() {
  local dir
  dir=$(find ${1:-.} -path '*/\.*' -prune \
        -o -type d -print 2> /dev/null | fzf +m) &&
  cd "$dir"
}


fkill() {
  local pid
  pid=$(ps -ef | sed 1d | fzf -m | awk '{print $2}')

  if [ "x$pid" != "x" ]
  then
    echo "$pid" | xargs kill -${1:-9}
  fi
}
