# ~/.zsh/themes/common.zsh

# ------------------------------------------------------------------------------
# Modified "common" prompt with functions from oh-my-zsh's "vi-mode"
# https://github.com/jackharrisonsherlock/common
# https://github.com/robbyrussell/oh-my-zsh/tree/master/plugins/vi-mode
# ------------------------------------------------------------------------------

setopt PROMPT_SUBST

PROMPT_SYMBOL=">"
MODE_INDICATOR="%{$fg[cyan]%}vi%{$reset_color%} "
PROMPT='$(host)$(current_dir)$(bg_jobs)$(return_status)'
RPROMPT='$(vi_mode)$(git_status)'

# Update editor info when keymap changes
zle-keymap-select() {
  zle reset-prompt
  zle -R
}

# Redraw prompt when terminal size changes
TRAPWINCH() {
  zle && { zle -R; zle reset-prompt }
}

zle -N zle-keymap-select

bg_jobs() {
  bg_status="%{$fg[yellow]%}%(1j.↓%j .)%{$reset_color%}"
  echo -n $bg_status
}

current_dir() {
  echo -n "%{$fg[blue]%}%c "
}

git_status() {
  local message=""
  local message_color="%F{green}"
  local staged=$(git status --porcelain 2>/dev/null | grep -e "^[MADRCU]")
  local unstaged=$(git status --porcelain 2>/dev/null | grep -e "^[MADRCU? ][MADRCU?]")

  if [[ -n ${staged} ]]; then
    message_color="%F{red}"
  elif [[ -n ${unstaged} ]]; then
    message_color="%F{yellow}"
  fi

  local branch=$(git rev-parse --abbrev-ref HEAD 2>/dev/null)
  if [[ -n ${branch} ]]; then
    message+="${message_color}${branch}%f"
  fi

  echo -n "${message}"
}

host() {
  if [[ -n $SSH_CONNECTION ]]; then
    me="%n@%m"
  elif [[ $LOGNAME != $USER ]]; then
    me="%n"
  fi
  if [[ -n $me ]]; then
    echo "%{$fg[green]%}$me%{$reset_color%}:"
  fi
}

return_status() {
  echo -n "%(?.%F{magenta}.%F{red})$PROMPT_SYMBOL%f "
}

vi_mode() {
  echo "${${KEYMAP/vicmd/$MODE_INDICATOR}/(main|viins)/}"
}
