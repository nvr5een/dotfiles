# ~/.zsh/antigen.zsh

if [[ ! -d ~/.antigen ]]; then
  git clone https://github.com/zsh-users/antigen.git ~/.antigen
  touch ~/.antigen/debug.log
fi

source ~/.antigen/antigen.zsh

antigen bundles <<EOBUNDLES
  zdharma/fast-syntax-highlighting
  zsh-users/zsh-history-substring-search
  zsh-users/zsh-completions
EOBUNDLES

antigen apply

bindkey '^P' history-substring-search-up
bindkey '^N' history-substring-search-down
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down
