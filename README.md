# dotfiles

+ Shell: [zsh](https://www.zsh.org/)
+ Editor: [neovim](https://neovim.io/)
+ Terminal Multiplexer: [tmux](https://github.com/tmux/tmux/wiki/)
+ Window Manager: [dwm](https://dwm.suckless.org/)
+ Terminal Emulator: [st](https://st.suckless.org)
+ File Manager: [vifm](https://vifm.info/)
+ Media Player: [mpv](https://mpv.io/)
+ Music Player: [cmus](https://cmus.github.io/)
+ Image Viewer: [sxiv](https://github.com/muennich/sxiv/)
+ Document Viewer: [zathura](https://pwmt.org/projects/zathura/)
