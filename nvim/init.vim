" ~/.config/nvim/init.vim

" Plugins {{{

let s:minpac_dir = '~/.config/nvim/pack/minpac/opt/minpac'
if has('vim_starting')
  if empty(glob(s:minpac_dir))
    echo "Installing minpac ..."
    execute '!git clone --depth 1 https://github.com/k-takata/minpac ' . s:minpac_dir
    autocmd VimEnter * source $MYVIMRC | call minpac#update('', {'do': 'call minpac#status()'})
  endif
endif

packadd minpac

call minpac#init()

call minpac#add('k-takata/minpac', {'type': 'opt'})
call minpac#add('tpope/vim-commentary')
call minpac#add('tpope/vim-surround')
call minpac#add('tpope/vim-repeat')
call minpac#add('tpope/vim-unimpaired')
call minpac#add('justinmk/vim-dirvish')
call minpac#add('airblade/vim-gitgutter')
call minpac#add('joereynolds/vim-minisnip')
call minpac#add('mtth/scratch.vim')
call minpac#add('plasticboy/vim-markdown')
call minpac#add('chase/vim-ansible-yaml')
call minpac#add('ekalinin/Dockerfile.vim')

" Plugin settings
let g:minisnip_dir = '~/.config/nvim/minisnip'
let g:vim_markdown_folding_style_pythonic = 1

" Define user commands for updating/cleaning plugins
command! PackUpdate packadd minpac | source $MYVIMRC | call minpac#update('', {'do': 'call minpac#status()'})
command! PackClean  packadd minpac | source $MYVIMRC | call minpac#clean()
command! PackStatus packadd minpac | source $MYVIMRC | call minpac#status()

" }}}
" Options {{{

set backup " keep backup files
set backupdir=~/.local/share/nvim/backup
set clipboard^=unnamed,unnamedplus " use system clipboard
set colorcolumn=+1 " highlight column set by 'textwidth' +1
set cpoptions+=$ " put a '$' at end of text being changed in current line
set cursorline " highlight cursorline
set expandtab " use spaces when inserting tabs
set nofoldenable " all folds start open
set hidden " allow hiding buffers with unsaved changes
set history=1000 " keep 1000 lines of command line history
set ignorecase " ignore case in search patterns
set lazyredraw " do not redraw screen while executing macros (performance)
set listchars=tab:>\ ,eol:¬,extends:>,precedes:<,nbsp:+ " :list strings
set modelines=0 " do not check modelines for commands (security)
set number " show line numbers
set omnifunc=syntaxcomplete#Complete " enable autocompletion
set pastetoggle=<F2> " toggles the 'paste' option
set path=.,** " search directory tree relative to directory of current file
set relativenumber " enable vertical motion commands (e.g. j k + -)
set scrolloff=5 " minimum number of lines kept above and below cursor
set shiftwidth=2 " number of spaces to use for each (auto)indent
set showbreak=↪  " string put at beginning of wrapped lines
set showmatch " highlight matching brackets
set smartcase " search is case-sensitive when pattern contains capital letters
set softtabstop=-1 " use shiftwidth value for tab and backspace
set textwidth=80 " wrap text at 80 characters
set timeoutlen=500 " milliseconds waited for mapped sequence to complete
set undofile " save undo history
set updatetime=100 " time waited after typing before updating signs (gitgutter)
set virtualedit=block " allow cursor to go anywhere in visual block mode

" Wildmenu
set wildcharm=<C-z> " used in mappings to invoke completion
set wildignorecase " case-insensitive completion
set wildignore+=*.git,*.DS_Store
set wildignore+=*.bmp,*.gif,*.jp*,*.png
set wildignore+=*.gz,*.rar,*.zip

" Grep
if executable('rg')
  set grepprg=rg\ --vimgrep\ --no-heading
elseif executable('ag')
  set grepprg=ag\ --nogroup\ --nocolor
endif

" Colors
colorscheme sourcerer

" }}}
" Statusline {{{

set statusline= " begin left align
set statusline+=%{GitForStatusline()}
set statusline+=%{PasteForStatusline()}
set statusline+=\ %F " file path
set statusline+=%m " modified?
set statusline+=%r " read only?
set statusline+=%= " begin right align
set statusline+=\ %y " file type
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=\ \|\ %{&fileformat}
set statusline+=\ [%p%% " percentage through file
set statusline+=\ %l:%c] " line:column

" }}}
" Mappings {{{

let mapleader="\<Space>"

" Escape
inoremap jk <Esc>
cnoremap jk <C-c>

" Yank to end of line
noremap Y y$

" Format current paragraph or visual selection with Q
nnoremap Q gqap
vnoremap Q gq

" Navigate wrapped lines with 'relativenumber' set
noremap <silent> <expr> j (v:count == 0 ? 'gj' : 'j')
noremap <silent> <expr> k (v:count == 0 ? 'gk' : 'k')

" Split windows
noremap <leader>- :sp<CR>
noremap <leader>\ :vsp<CR>

" Circular window navigation
nnoremap <tab> <C-w>w
nnoremap <S-tab> <C-w>W

" Toggle previous buffer
nnoremap <leader><Space> :e#<CR>

" Use <C-L> to clear the highlighting of :set hlsearch.
if maparg('<C-L>', 'n') ==# ''
  nnoremap <silent> <C-L> :nohlsearch<C-R>=has('diff')?'<Bar>diffupdate':''<CR><CR><C-L>
endif

" Native alternatives to plugins
" https://www.vi-improved.org/recommendations/
" https://stackoverflow.com/questions/16082991/vim-switching-between-files-rapidly-using-vanilla-vim-no-plugins
nnoremap <leader>a :argadd <C-r>=fnameescape(expand('%:p:h'))<CR>/*<C-d>
nnoremap <leader>b :buffer <C-z><S-Tab>
nnoremap <leader>e :e <C-r>=fnameescape(expand('%:p:h'))<CR>/*<C-d>
nnoremap <leader>f :find *
nnoremap <leader>g :grep<Space>
nnoremap <leader>G :grep <C-r><C-w><CR>
nnoremap <leader>sw :call StripWhitespace()<CR>

" Close all but current fold
nnoremap <leader>z zMzvzz

" Replace all occurrences of word under cursor
nnoremap <leader>r :%s/\<<C-r><C-w>\>//g<Left><Left>

" sudo write
cnoremap w!! w !sudo tee > /dev/null %

" Edit and source vimrc
nnoremap <silent> <leader>v :e $MYVIMRC<CR>
nnoremap <silent> <leader>sv :so $MYVIMRC<CR>

" Notes
if !empty(glob('~/projects/notes'))
  nnoremap <leader>n :e ~/projects/notes/
endif

" }}}
" Functions {{{

function! GitBranch() abort
  return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endfunction

function! GitForStatusline() abort
  let l:branchname = GitBranch()
  return strlen(l:branchname) > 0?'['.l:branchname.']':''
endfunction

function! PasteForStatusline() abort
  let l:paste_status=&paste
  if l:paste_status == 1
    return '[paste]'
  else
    return ''
  endif
endfunction

function! StripWhitespace() abort
  let save_cursor = getpos(".")
  let old_query = getreg('/')
  %s/\s\+$//e
  call setpos('.', save_cursor)
  call setreg('/', old_query)
endfunction

" }}}
" Autocommands {{{

if has("autocmd")

  augroup general
    autocmd!
    autocmd BufRead,BufNewFile Vagrantfile set ft=ruby
    autocmd BufRead,BufNewFile *vifmrc*,*.vifm set ft=vim
    autocmd BufRead,BufNewFile *hosts*,*.yml set ft=ansible
    autocmd BufWinEnter *.txt if &ft == 'help' | wincmd L | endif
    autocmd VimResized * :wincmd = " Resize splits when window is resized
    autocmd FileType css,less,scss,javascript,java
          \ setlocal foldmethod=marker foldmarker={,}
    autocmd Filetype sh,vim,zsh setlocal foldmethod=marker
    autocmd Filetype ruby setlocal foldmethod=syntax
    autocmd Filetype python setlocal foldmethod=indent tabstop=4
  augroup END

  " When editing a file, always jump to the last known cursor position.
  augroup cursor_position
    autocmd!
    autocmd BufReadPost *
      \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
      \ |   exe "normal! g`\""
      \ | endif
  augroup END

  " Open quickfix window after :make, :grep, etc.
  augroup auto_quick_fix
    autocmd!
    autocmd QuickFixCmdPost [^l]* cwindow
    autocmd QuickFixCmdPost l*    lwindow
  augroup END

endif " has("autocmd")

" }}}
